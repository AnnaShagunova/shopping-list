# Shopping-List
The shopping list app

## Steps:

step 1 - Creating a controller, connecting DB

step 2 - Implementation of the main functionality: 
        adding to the list, 
        changing the status of an item (purchased / not purchased), 
        deleting marked items, 
        clearing the list.

step 3 - Sorting the list

step 4 - Form design

