package com.example.shoppinglist.controllers;

import com.example.shoppinglist.models.Item;
import com.example.shoppinglist.repos.ItemRepo;
import com.example.shoppinglist.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/list")
public class MainController {

    private final ItemRepo repo;
    public final ItemService service;

    @Autowired
    public MainController(ItemRepo repo, ItemService service) {
        this.repo = repo;
        this.service = service;
    }

    @GetMapping()
    public String newItem(Model model) {
        model.addAttribute("item", new Item());
        model.addAttribute("items", service.sorted());
        return "/list";
    }

    @PostMapping("/add")
    public String addItem(@ModelAttribute("item") Item item) {
        repo.save(item);
        return "redirect:/list";
    }

    @PostMapping("/check/{id}")
    public String setCheckbox(@PathVariable("id") Long id) {
        service.setCheckbox(id);
        return "redirect:/list";
    }

    //Очистка списка
    @PostMapping("/delall")
    public String deleteAll() {
        repo.deleteAll();
        return "redirect:/list";
    }

    //Удаление item.isCheck() = true
    @PostMapping("/delcheck")
    public String deleteTrue() {
        service.deleteTrue();
        return "redirect:/list";
    }

}
