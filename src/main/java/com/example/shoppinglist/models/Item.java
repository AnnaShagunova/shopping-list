package com.example.shoppinglist.models;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.util.Comparator;

@Entity(name = "Item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Length(max = 100)
    private String text;

    @NotBlank
    @Length(max = 100)
    private String tag;

    private boolean check;

    public Item() {
    }

    public Item(String text, String tag, boolean check) {
        this.text = text;
        this.tag = tag;
        this.check = check;
    }

    public Item(Long id, String text, String tag) {
        this.id = id;
        this.text = text;
        this.tag = tag;
        this.check = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }



    public static Comparator<Item> tegComparator = new Comparator<Item>() {

        @Override
        public int compare(Item o1, Item o2) {
            return o1.tag.compareTo(o2.tag);
        }

    };
}
