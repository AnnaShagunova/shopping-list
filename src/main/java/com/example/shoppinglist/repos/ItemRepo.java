package com.example.shoppinglist.repos;

import com.example.shoppinglist.models.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface ItemRepo extends CrudRepository<Item, Long> {

    List<Item>  findAll();

}
