package com.example.shoppinglist.service;

import com.example.shoppinglist.models.Item;
import com.example.shoppinglist.repos.ItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {

    private final ItemRepo repo;

    @Autowired
    public ItemService(ItemRepo repo) {
        this.repo = repo;
    }


    public List<Item> sorted() {
        return repo.findAll().stream().sorted(Item.tegComparator).collect(Collectors.toList());
    }

    public void setCheckbox(Long id) {
        Item item = repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Item " + id + " is not found"));
        item.setCheck(true);
        repo.save(item);
    }


    public void deleteTrue() {
        List<Item> items = repo.findAll();
        items.stream().filter(Item::isCheck).forEach(item -> repo.deleteById(item.getId()));
    }



}
